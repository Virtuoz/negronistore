class Order < ActiveRecord::Base
	has_many :line_items, dependent: :destroy
	validates :name, :address, :email,  presence: true
	validates :phone, numericality: { only_integer: true }
	validates :email, confirmation: true
  validates_format_of :email, :with => /@/
  
  
	
	def add_line_items_from_cart(cart)
		cart.line_items.each do |item|
			item.cart_id = nil
			line_items << item
		end
	end

end

json.extract! @product, :id, :name, :price, :ingridient, :description, :image, :category_id, :created_at, :updated_at

class OrderNotifier < ApplicationMailer
  default from: 'Negroni <depot@example.com>'

  # Subject can be set in your I18n file at config/locales/en.yml
  # with the following lookup:
  #
  #   en.order_notifier.received.subject
  #
  def received(order)
    @order = order
    mail to: order.email,  bcc: ["order.negroni@gmail.com","vitaliinegroni@mail.ru","pavel.serega@mail.ru", "andrei.richi@gmail.com"], subject: 'Подтверждение заказа в Negroni Store'
end

  # Subject can be set in your I18n file at config/locales/en.yml
  # with the following lookup:
  #
  #   en.order_notifier.shipped.subject
  #
  def shipped(order)
    @order = order
    mail to: order.email, subject: 'Заказ из Negroni Store отправлен'
end
end

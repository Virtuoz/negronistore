class CreateProducts < ActiveRecord::Migration
  def change
    create_table :products do |t|
      t.string :name
      t.decimal :price
      t.string :ingridient
      t.string :description
      t.string :image
      t.integer :category_id

      t.timestamps null: false
    end
  end
end
